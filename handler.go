package urlshort

import (
	"encoding/json"
	"net/http"

	yaml "gopkg.in/yaml.v2"
)

// MapHandler will return an http.HandlerFunc that will attempt
// to map any paths (keys in the map) to their corresponding
// URL (map key values).
// If the path is not provided in the map, then the fallback
// http.Handler will be called instead.
func MapHandler(pathsToUrls map[string]string, fallback http.Handler) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		path := request.URL.Path

		// If our request path has a key which matches our mapped paths...
		if dest, ok := pathsToUrls[path]; ok {
			http.Redirect(writer, request, dest, http.StatusFound)

			return
		}

		// else...
		fallback.ServeHTTP(writer, request)
	}
}

// JsonHandler will parse the provided YAML and then return
// an http.HandlerFunc
//
// Json is expected to be in the format:
// [
//   {"path": "/some-path", "url": "http://some-url.com"}
// ]
func JsonHandler(bytes []byte, fallback http.Handler) (http.HandlerFunc, error) {
	var pathUrls []PathUrl
	error := json.Unmarshal(bytes, &pathUrls)

	if error != nil {
		return nil, error
	}

	pathsToUrls := buildUrlMap(pathUrls)

	return MapHandler(pathsToUrls, fallback), nil
}

// YAMLHandler will parse the provided YAML and then return
// an http.HandlerFunc
//
// YAML is expected to be in the format:
//
//- path: /some-path
//  url: https://some-url.com
func YAMLHandler(bytes []byte, fallback http.Handler) (http.HandlerFunc, error) {
	pathUrls, error := parseYaml(bytes)

	if error != nil {
		return nil, error
	}

	pathsToUrls := buildUrlMap(pathUrls)

	return MapHandler(pathsToUrls, fallback), nil
}

func parseYaml(bytes []byte) ([]PathUrl, error) {
	var pathUrls []PathUrl
	error := yaml.Unmarshal(bytes, &pathUrls)

	if error != nil {
		return nil, error
	}

	return pathUrls, nil
}

func buildUrlMap(pathUrls []PathUrl) map[string]string {
	pathsToUrls := make(map[string]string)
	for _, pathUrl := range pathUrls {
		pathsToUrls[pathUrl.Path] = pathUrl.URL
	}

	return pathsToUrls
}

type PathUrl struct {
	Path string `yaml:"path"`
	URL  string `yaml:"url"`
}
