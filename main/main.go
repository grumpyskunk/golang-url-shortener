package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"

	"gitlab.com/grumpyskunk/urlshort"
)

func main() {
	jsonFile := flag.String("json", "", "A json file containing a series of shortened URL definitions.")
	yamlFile := flag.String("yaml", "", "A yaml file containing a series of shortened URL definitions.")
	flag.Parse()

	mux := defaultMux()

	// Build the MapHandler using the mux as the fallback
	pathsToUrls := map[string]string{
		"/urlshort-godoc": "https://godoc.org/github.com/gophercises/urlshort",
		"/yaml-godoc":     "https://godoc.org/gopkg.in/yaml.v2",
	}
	mapHandler := urlshort.MapHandler(pathsToUrls, mux)
	topHandler := &mapHandler

	// If we have a json file, build the JsonHandler using the current topHandler as the fallback
	topHandler, error := handleJsonFile(*jsonFile, topHandler)
	if error != nil {
		panic(error)
	}

	// If we have a yaml file, build the YAMLHandler using the current topHandler as the fallback
	topHandler, error = handleYamlFile(*yamlFile, topHandler)
	if error != nil {
		panic(error)
	}

	fmt.Println("Listing on http://localhost:8080")
	http.ListenAndServe(":8080", topHandler)
}

func handleJsonFile(jsonFile string, topHandler *http.HandlerFunc) (*http.HandlerFunc, error) {
	if jsonFile != "" {
		bytes, error := openFile(jsonFile)

		if error != nil {
			return nil, error
		}

		jsonHandler, error := urlshort.JsonHandler(bytes, topHandler)

		if error != nil {
			return nil, error
		}

		fmt.Printf("YAML map file (%s) imported successfully.\n", jsonFile)

		topHandler = &jsonHandler
	}

	return topHandler, nil
}

func handleYamlFile(yamlFile string, topHandler *http.HandlerFunc) (*http.HandlerFunc, error) {
	if yamlFile != "" {
		bytes, error := openFile(yamlFile)

		if error != nil {
			return nil, error
		}

		yamlHandler, error := urlshort.YAMLHandler(bytes, topHandler)

		if error != nil {
			return nil, error
		}

		fmt.Printf("YAML map file (%s) imported successfully.\n", yamlFile)

		topHandler = &yamlHandler
	}

	return topHandler, nil
}

func openFile(filename string) ([]byte, error) {
	bytes, error := os.ReadFile(filename)

	if error != nil {
		return nil, error
	}

	return bytes, nil
}

func defaultMux() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/", hello)
	return mux
}

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello, world!")
}
